<?php
if(!isset($_SESSION) )session_start();
include_once('../../vendor/autoload.php');

use App\BITM\SEIP\User\User;
use App\BITM\SEIP\User\Auth;
use App\BITM\SEIP\Message\Message;
use App\BITM\SEIP\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}
?>


<!DOCTYPE HTML>
<html>
<head>
    <title>HTMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


    <!-- Bootstrap Core CSS -->
    <link href="../../resource/assets/bootstrap/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="../../resource/assets/css/styleadmin.css" rel='stylesheet' type='text/css' />
    <!-- Graph CSS -->
    <link href="../../resource/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- jQuery -->

    <!-- lined-icons -->
    <link rel="stylesheet" href="../../resource/assets/css/icon-font.min.css" type='text/css' />

    <!-- //lined-icons -->
    <script src="../../resource/assets/js/jquery-1.10.2.min.js"></script>

</head>
<body>
<div class="page-container">
    <!--/content-inner-->
    <div class="left-content">
        <div class="inner-content">
            <!-- header-starts -->
            <div class="header-section">
                <!-- top_bg -->
                <div class="top_bg">
                    <div class="header_top">
                        <div class="top_right">
                            <ul>
                                <li><a href="User/index.php">User</a></li>|
                                <li><a href="#">TITILE</a></li>|
                                <li><a href="#">TITILE</a></li>
                            </ul>
                        </div>
                        <div class="top_left">
                            <h2><span></span><a class="fa-2x" href="User/Authentication/logout.php">Log Out</a></h2>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <!-- end top_bg -->
                <table align="center">
                    <tr>
                        <td height="100" >

                            <div id="message" >

                                <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                                    echo "&nbsp;".Message::message();
                                }
                                Message::message(NULL);

                                ?>
                            </div>

                        </td>
                    </tr>
                </table>
                <!-- header-Ends -->


















                    </div>
                </div>
            </div>

            <!--/content-inner-->

            <!--/sidebar-menu-->
            <div class="sidebar-menu">
                <header class="logo1">
                    <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a>
                </header>
                <div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
                <div class="menu">
                    <ul id="menu" >
                        <li><a href="index.html"><i class="fa fa-tachometer"></i> <span>Home</span></a></li>
                        <li id="menu-academico" ><a href="#"><i class="fa fa-table"></i> <span> BUILDING </span> <span class="fa fa-angle-right" style="float: right"></span></a>
                            <ul id="menu-academico-sub" >
                                <li id="menu-academico-avaliacoes" ><a href="Admin/createBuilding.php">ADD</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="#">EDIT</a></li>
                                <li id="menu-academico-boletim" ><a href="#">DELETE</a></li>
                                <li id="menu-academico-boletim" ><a href="#">VIEW</a></li>
                            </ul>
                        </li>
                        <li id="menu-academico" ><a href="#"><i class="fa fa-table"></i> <span> Circle </span> <span class="fa fa-angle-right" style="float: right"></span></a>
                            <ul id="menu-academico-sub" >
                                <li id="menu-academico-avaliacoes" ><a href="Admin/createCircle.php">ADD</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="#">EDIT</a></li>
                                <li id="menu-academico-boletim" ><a href="#">DELETE</a></li>
                                <li id="menu-academico-boletim" ><a href="#">VIEW</a></li>
                            </ul>
                        </li>
                        <li id="menu-academico" ><a href="#"><i class="fa fa-table"></i> <span> TAX INFO </span> <span class="fa fa-angle-right" style="float: right"></span></a>
                            <ul id="menu-academico-sub" >
                                <li id="menu-academico-avaliacoes" ><a href="#">ADD</a></li>
                                <li id="menu-academico-avaliacoes" ><a href="#">EDIT</a></li>
                                <li id="menu-academico-boletim" ><a href="#">DELETE</a></li>
                                <li id="menu-academico-boletim" ><a href="#">VIEW</a></li>
                            </ul>
                        </li>
                        <li id="menu-academico" ><a href="shoes.html"><i class="lnr lnr-book"></i> <span>TAX CIRCLE</span></a></li>
                        <li id="menu-academico" ><a href="shoes.html"><i class="lnr lnr-book"></i> <span>SMS</span></a></li>
                        <li id="menu-academico" ><a href="shoes.html"><i class="lnr lnr-book"></i> <span>NOTICE</span></a></li>
                        <li id="menu-academico" ><a href="shoes.html"><i class="lnr lnr-book"></i> <span>PENALTY</span></a></li>

                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <script>
            var toggle = true;

            $(".sidebar-icon").click(function() {
                if (toggle)
                {
                    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
                    $("#menu span").css({"position":"absolute"});
                }
                else
                {
                    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
                    setTimeout(function() {
                        $("#menu span").css({"position":"relative"});
                    }, 400);
                }

                toggle = !toggle;
            });
        </script>


</body>
</html>