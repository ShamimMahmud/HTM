<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Holding Tax Calculator</title>
		<link href="../../../resource/assets/css/stylecalcuator.css" rel="stylesheet" type="text/css">
		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
		<link href="http://pg.gaarsam.com//html/assets/bootstrap/css/bootstrap.css" media="screen" rel="stylesheet" type="text/css" >
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
		<script type="text/javascript" src="http://pg.gaarsam.com//html/assets/js/jquery-2.1.4.min.js"></script>
		<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
		<script type="text/javascript" src="http://pg.gaarsam.com//html/assets/bootstrap/js/bootstrap.min.js"></script>
		<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>-->
		<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>-->
		<script type="text/javascript" src="http://pg.gaarsam.com//html/assets/js/jquery.validate.min.js"></script>
		<style>
			.wrap { max-width: 980px; margin: 10px auto 0; }
			#steps { margin: 80px 0 0 0 }
			.commands { overflow: hidden; margin-top: 30px; }
			.prev {float:left}
			.next, .submit {float:right}
			.error { color: #b33; }
			#progress { position: relative; height: 5px; background-color: #eee; margin-bottom: 20px; }
			#progress-complete { border: 0; position: absolute; height: 5px; min-width: 10px; background-color: #337ab7; transition: width .2s ease-in-out; }
		</style>
		<link rel="stylesheet" href="homeloan.css">
		<script src="../../../resource/assets/js/calculator.js" type="text/javascript"></script>
		<script>
        $( function() {
            var $HomeLoan = $( '#HomeLoan' );
            $HomeLoan.validate({errorElement: 'em'});
            $HomeLoan.formToWizard({
                submitButton: 'SaveAccount',
                nextBtnClass: 'btn btn-primary next',
                prevBtnClass: 'btn btn-default prev',
                buttonTag:    'button',
                validateBeforeNext: function(form, step) {
                    var stepIsValid = true;
                    var validator = form.validate();
                    $(':input', step).each( function(index) {
                        var xy = validator.element(this);
                        stepIsValid = stepIsValid && (typeof xy == 'undefined' || xy);
                    });
                    return stepIsValid;
                },
                progress: function (i, count) {
                    $('#progress-complete').width(''+(i/count*100)+'%');
                }
            });
        });
		</script>
		<style>
		
		.jumbotron {
    position: relative;
    background: #000 url("https://s14.postimg.org/brfoy05nl/homeloan.jpg") center center;
    width: 100%;
    height: 100%;
    background-size: cover;
    overflow: hidden;
}

		</style
    
</head>

<body style="font-family: 'Montserrat Light', sans-serif;">
<div class="jumbotron text-center">
  <h2 style="color:White;">Holding Tax Calculator</h2>
</div>

<div class="container">

<div class="tab-content col-md-10">
        <div class="tab-pane active" id="tab_a">
		<script language="javascript" type="text/javascript">
		function eligiable()
        {
                var P1 = document.formval2.pr_amt2.value; // pick the form input value..
                var rate1 = document.formval2.int_rate2.value; // pick the form input value..
                var n1 = document.formval2.period2.value; // pick the form input value..
                var r1 = rate1/(12*100); // to calculate rate percentage..
                var prate1 = (P1 * r1 * Math.pow((1+r1),n1*12))/(Math.pow((1+r1),n1*12)-1); // to calculate compound interest..
                 var emi1 = Math.ceil(prate1 * 100) / 100; // to parse emi amount..
				var existing = document.formval2.ExLoan.value;
				var existingLoan=(existing-(existing*60/100));
				var income1 = document.formval2.NetIncome.value;
				if (income1<=14999){
				var incomere=((income1)*40/100)-existingLoan;
				} else if (income1<=29999){
				var incomere=((income1)*45/100)-existingLoan;
				} else if (income1>=30000){
				var incomere=((income1)*50/100)-existingLoan;
				}
				var incomereq=Math.floor(incomere / emi1 * P1);
				var prate2 = (incomereq * r1 * Math.pow((1+r1),n1*12))/(Math.pow((1+r1),n1*12)-1); // to calculate compound interest2..
                 var emi2 = Math.ceil((prate2) * 100) / 100; // to parse emi2 amount..   //Check again Reminder
                // to assign value in field1 as fixed upto two decimal..
                if (incomereq > P1) {
				document.formval3.field13.value = ("You are Eligible for this loan ");
				document.formval3.field11.value = (("? "+P1+" at EMI "+"? "+emi1.toFixed(0)));
				document.formval3.field12.value = ("You are Eligible for a maximum loan of "+("? "+incomereq+" at EMI "+"? "+emi2.toFixed(0)));
				} else {
				document.formval3.field13.value = ("You are not Eligible for this loan");
				document.formval3.field11.value = ("");
				document.formval3.field12.value = ("You are Eligible for a maximum loan of "+("? "+incomereq+" at EMI "+"? "+emi2.toFixed(0)));
				}
				
         //to assign value in field2.. 
         } 
         </script>
				<form name="formval2" class="form-horizontal col-md-8" style="font-size:12px;">
					<div class="form-group">
						<label for="input" class="col-sm-4 control-label">CIRCLE :</label>
						<div class="col-sm-8">
						<div class="input-group">
						<span class="input-group-addon">?</span>
						<select class="form-control input-sm" id="purpose">
							<option value="" disabled selected>Select Cirle</option>
						 <option>CIRCLE 1</option>
						 <option>CIRCLE 2</option>
						 <option>CIRCLE 3</option>
						 <option>CIRCLE 4</option>
							<option>CIRCLE 5</option>
						</select>
						</div>
						</div>
					</div>
					<!--<script type="text/javascript">
						function ShowHideDiv() {
							var chkYes = document.getElementById("chkYes");
							var dvFinalized = document.getElementById("dvFinalized");
							dvFinalized.style.display = chkYes.checked ? "block" : "none";
						}
					</script>-->
					
					<div class="form-group">
						<label for="input" class="col-sm-4 control-label">WARD :</label>
						<div class="col-sm-8">
						<div class="input-group">
						<span class="input-group-addon">?</span>
						<select class="form-control input-sm" id="purpose">
							<option value="" disabled selected>Select Ward</option>
						 <option>CIRCLE 1</option>
						 <option>CIRCLE 2</option>
						 <option>CIRCLE 3</option>
						 <option>CIRCLE 4</option>
							<option>CIRCLE 5</option>
						</select>
						</div>
						</div>
					</div>
					<div class="form-group">
						<label for="input" class="col-sm-4 control-label">MOHOLLA :</label>
						<div class="col-sm-8">
						<div class="input-group">
						<span class="input-group-addon">?</span>
						<select class="form-control input-sm" id="purpose">
							<option value="" disabled selected>Select Moholla</option>
						<option>CIRCLE 1</option>
						<option>CIRCLE 2</option>
						<option>CIRCLE 3</option>
						<option>CIRCLE 4</option>
							<option>CIRCLE 5</option>
					 	</select>
						</div>
						</div>
					</div>
					
					<div class="form-group">
						<label for="input" class="col-sm-4 control-label">Percentage of TAX</label>
						<div class="col-sm-8">
						<div class="input-group">
						<input type="number" class="form-control input-sm" id="input" disabled value="9.5" name="int_rate2">
						<span class="input-group-addon">%</span>
						</div>
						</div>
					</div>
					<div class="form-group">
							<label for="input" class="col-sm-4 control-label">BUILDING AREA SIZE</label>
							<div class="col-sm-8">
							<div class="input-group">
							<span class="input-group-addon">?</span>
							<input type="number" class="form-control input-sm" name="HomeLoanAmount" id="HomeLoanAmount" placeholder="Enter Area Size"/>
							<span class="input-group-addon">Sq.Ft.</span>
							</div>
							</div>
					</div>
					<div class="form-group">
						<label for="input" class="col-sm-4 control-label">TENANT CONDITION</label>
						<div class="col-sm-8">
						<label class="radio-inline"><input type="radio" name="optradio">OWN USE</label>
						<label class="radio-inline"><input type="radio" name="optradio">RENTAL</label>
						</div>
						</div>

					<div class="form-group">
						<label for="input" class="col-sm-4 control-label">Building Types</label>
						<div class="col-sm-8">
							<label class="radio-inline"><input type="radio" name="optradio">Building</label>
							<label class="radio-inline"><input type="radio" name="optradio">Semi-Pacca</label>
							<label class="radio-inline"><input type="radio" name="optradio">Tin Shed</label>
						</div>
					</div>

					</div>


					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-12">
						<button type="button" class="btn btn-primary" onclick="eligiable()">Calculate Tax</button>
						<button type="reset" class="btn btn-link">Reset All</button>
						</div>
					</div>
				</form>
				
		
    
				

</div>
	
	

		
</div>
</div>
</div>




</body>
</html>
