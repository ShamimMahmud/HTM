<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HTMS</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../resource/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../resource/assets/css/userIndexStyle.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../resource/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

 
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><b style="font-family:">HTMS</b></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="services.html"><span class="glyphicon glyphicon-home"></span> Home</a>
                    </li>
					
					<!--
                    
					<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-usd"></span> Tax <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="portfolio-1-col.html">USER INFORMATION</a>
                            </li>
                            <li>
                                <a href="portfolio-2-col.html">BALANCE REPORT</a>
                            </li>
                            <li>
                                <a href="portfolio-3-col.html">PAYMENT</a>
                            </li>
                            
                        </ul>
						
                    </li>-->
                    <li>
                        <a href="../Menu/calculator.php"><span class="glyphicon glyphicon-file"></span> Calculator</a>
                    </li>
                    <li>
                        <a href="contact.html"><span class="glyphicon glyphicon-file"></span> Notice</a>
                    </li>
					<li>
                        <a href="about.html"><span class="glyphicon glyphicon-info-sign"></span> About Us</a>
                    </li>
					<li>
                        <a href="contact.html"><span class="glyphicon glyphicon-phone-alt"></span> Contact Us</a>
                    </li>
					
                    
                    <li>
                        <a href="contact.html"><span class="glyphicon glyphicon-log-in"></span> Log In</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('../../../resource/assets/img/city/cheragi_pahar.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 1</h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('../../../resource/assets/img/city/port.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 2</h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('../../../resource/assets/img/city/bridge.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 3</h2>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Welcome to Holding Tax Management System
                </h1>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"></i>USER INFORMATION</h4>
                    </div>
                    <div class="panel-body">
                        <p style="text-align:justify">ONLY REGISTERED USER CAN CHECK THE VALID INFORMATION WHOSE ARE THE OWNER OF A BUILDING IN CHITTAGONG CITY CORPORATION AREA. </p>
                        <a href="#" class="btn btn-default">LOG IN</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-gift"></i> BALANCE REPORT</h4>
                    </div>
                    <div class="panel-body">
                        <p style="text-align:justify">BALANCE REPORT PROVIDES THE INFORMATION OF TRANSACTION LIST OR LAST PAYED BILLING INFORMATION. TO CHECK DETAILS FIRST LOG IN...</p>
                        <a href="#" class="btn btn-default">LOG IN</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i> PAYMENT</h4>
                    </div>
                    <div class="panel-body">
                        <p style="text-align:justify">HTMS PROVIDES THE BEST PAYMENT METHOD TO THE USER TO PAY TAX IN EASIEST WAY LIKE BANK PAYMENT OR MOBILE BANKING (BKASH , SURECASH , UCASH , MCASH)</p>
                        <a href="#" class="btn btn-default">LOG IN</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Project Running</h2>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="../../../resource/assets/img/city/dustbin.jpg" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="../../../resource/assets/img/city/billboard.jpg" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="../../../resource/assets/img/city/road.jpg" alt="">
                </a>
            </div>
            <!--<div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>-->
        </div>
        <!-- /.row -->

        <!-- Features Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Message From Mayor</h2>
            </div>
            <div class="col-md-6">
                <p><h3>Welcome To Chittagong City Corporation</h3></p>
                 
                <p style="text-align:justify">Chittagong City Corporation descended from the name of Chittagong Municipality which was founded on June 22, 1863. The initial area of newly formed Municipality was only 6 Sq. Miles. There were 5 wards viz A, B, C, D and E. Mr. J. D. Ward was first appointed administrator. Late Khan Bahadur Abdus Sattar was the first elected Chairman of Chittagong Municipality. Late Nur Ahmed was the most significant Chairman in the life of Municipality who introduced the important Education System under Municipal supervision. He governed the organization for 30 years.<br>
                On June 27, 1977 Chittagong Municipality was renamed as Chittagong Paura-Shava and Late Fazal Karim became the first elected Chairman of Paura-Shava.<br> 
                On September 16, 1982 the Paura-Shava was upgraded to Municipal Corporation and Brigadier Mofizur Rahman Chowdhury was appointed as the Administrator while Late Fazal Karim became the Sub-Administrator.<br>
                Later on July 31, 1990 it was renamed as Chittagong City Corporation and government appointed Mr. Mahmudul Islam Chowdhury as Mayor. Later Mr. Mir Mohammed Nasiruddin ascended as Mayor and continued until the first election in 1994. Mr. A.B.M. Mohiuddin Chowdhury became the first democratically elected Mayor in the history of Chittagong City Corporation. Alhaj M. Manjur Alam was elected as Mayor in the City Corporation election held in 2010. AJM Nasir Uddin was elected in the last City Corporation election held in 28 April 2015. Currently the City Corporation area is divided into 41 wards.</p>
            </div>
            <div class="col-md-offset-2 col-md-4">
                <img class="img-responsive" src="../../../resource/assets/img/city/mayor_ctg.png" alt="">
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Call to Action Section -->
        <div class="well">
            <div class="row">
                <div class="col-md-8">
                    <p>THIS IS THE BETA VERSION PROJECT OF CHITTAGONG CITY CORPORATION TAX MANAGEMENT SYSTEM. IF ANY KIND OF UPDATE OR QUERY, FEEL FREE TO ASK.</p>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-lg btn-default btn-block" href="#">Call US</a>
                </div>
            </div>
        </div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy;TEAM: PHP_Learner'S Developed, 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="../../../resource/assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
