
<!DOCTYPE HTML>
<html>
<head>
<title>HTMS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<!-- Bootstrap Core CSS -->
<link href="../../../resource/assets/bootstrap/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="../../../resource/assets/css/styleadmin.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="../../../resource/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<!-- jQuery -->

<!-- lined-icons -->
<link rel="stylesheet" href="../../../resource/assets/css/icon-font.min.css" type='text/css' />

<!-- //lined-icons -->
<script src="../../../resource/assets/js/jquery-1.10.2.min.js"></script>

</head> 
<body>
   <div class="page-container">
   <!--/content-inner-->
	<div class="left-content">
	   <div class="inner-content">
		<!-- header-starts -->
			<div class="header-section">
			<!-- top_bg -->
						<div class="top_bg">
								<div class="header_top">
									<div class="top_right">
										<ul>
											<li><a href="../User/index.php">User</a></li>|
											<li><a href="#">TITILE</a></li>|
											<li><a href="#">TITILE</a></li>
										</ul>
									</div>
									<div class="top_left">
										<h2><span></span> Call us : 01670377276</h2>
									</div>
										<div class="clearfix"> </div>
								</div>
						</div>
				<!-- end top_bg -->
			
			<!-- header-Ends -->

<div class="content">
	<div class="women_main">
	<!-- start content -->
		
				


<div class="panel panel-widget forms-panel">
						<div class="form-title">
									<center><h2>BUILDING INFORMATION INSERT</h2></center>
								</div>
						<div class="forms">
								<div class="form-three widget-shadow">
									<form class="form-horizontal" enctype="multipart/form-data" method="post" action="store.php">
										<div class="form-group">
											<label class="col-sm-2 control-label">REGISTRATION DATE</label>
											<div class="col-sm-8">
												<input type="date" class="form-control1" name="reg_date" id="focusedinput" placeholder="Default Input">
											</div>
											<div class="col-sm-2">
												<p class="help-block">text!</p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">REGISTRATION No.</label>
											<div class="col-sm-8">
												<input type="text" class="form-control1" name="reg_no" id="focusedinput" placeholder="Default Input">
											</div>
											<div class="col-sm-2">
												<p class="help-block">text!</p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">BUILDING NAME</label>
											<div class="col-sm-8">
												<input type="text" class="form-control1" name="b_name" id="focusedinput" placeholder="Default Input">
											</div>
											<div class="col-sm-2">
												<p class="help-block">Name Of the Building</p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">AREA NAME</label>
											<div class="col-sm-8">
												<input type="text" class="form-control1" name="area_name" id="focusedinput" placeholder="Default Input">
											</div>
											<div class="col-sm-2">
												<p class="help-block">MUST BE IN SQ.FT</p>
											</div>
										</div>
										
										<div class="form-group">
											<label for="radio" class="col-sm-2 control-label">BUILDING TYPE</label>
											<div class="col-sm-8">
												<div class="radio-inline1"><label><input name="b_type" value="BUILDING" type="radio"> BUILDING </label></div>
												<div class="radio-inline1"><label><input name="b_type" value="SEMI-PACCA" type="radio"> SEMI-PACCA </label></div>
												<div class="radio-inline1"><label><input name="b_type" value="TIN-SHED" type="radio"> TIN-SHED </label></div>
											</div>
										</div>
										<div class="form-group">
											<label for="radio" class="col-sm-2 control-label">TENANT CONDITION</label>
											<div class="col-sm-8">
												<div class="radio-inline1"><label><input name="tenant_type" value="RENTAL" type="radio"> RENTAL</label></div>
												<div class="radio-inline1"><label><input name="tenant_type" value="OWN USE" type="radio"> OWN USE</label></div>
											</div>
										</div>

										<div class="form-group">
											<label for="radio" class="col-sm-2 control-label">BLOCK</label>
											<div class="col-sm-8">
												<div class="radio-inline"><label><input name="block" value="A" type="radio"> A</label></div>
												<div class="radio-inline"><label><input name="block" value="B" type="radio"> B</label></div>
												<div class="radio-inline"><label><input name="block" value="C" type="radio"> C</label></div>
												<div class="radio-inline"><label><input name="block" value="D" type="radio"> D</label></div>
												<div class="radio-inline"><label><input name="block" value="E" type="radio"> E</label></div>
												<div class="radio-inline"><label><input name="block" value="F" type="radio"> F</label></div>
												<div class="radio-inline"><label><input name="block" value="G" type="radio"> G</label></div>
												<div class="radio-inline"><label><input name="block" value="H" type="radio"> H</label></div>
												<div class="radio-inline"><label><input name="block" value="I" type="radio"> I</label></div>
												<div class="radio-inline"><label><input name="block" value="J" type="radio"> J</label></div>
												<div class="radio-inline"><label><input name="block" value="K" type="radio"> K</label></div>
												<div class="radio-inline"><label><input name="block" value="L" type="radio"> L</label></div>
											</div>
										</div>
										<div class="form-group">
											<label for="selector1" class="col-sm-2 control-label">CIRCLE LIST</label>
											<div class="col-sm-8">
											<select name="circle" class="form-control1">
												<option value="circle1">CIRCLE 1</option>
												<option value="circle2">CIRCLE 3</option>
												<option value="circle3">CIRCLE 2</option>
												<option value="circle4">CIRCLE 4</option>
												<option value="circle5">CIRCLE 5</option>
												<option value="circle6">CIRCLE 6</option>
												<option value="circle7">CIRCLE 7</option>
												<option value="circle8">CIRCLE 8</option>
											</select>
											</div>
										</div>
										<div class="form-group">
											<label for="selector1" class="col-sm-2 control-label">WORD lIST</label>
											<div class="col-sm-8"><select name="ward" id="selector1" class="form-control1">
												<option value="ward1">WARD 1</option>
												<option value="ward2">WARD 3</option>
												<option value="ward3">WARD 2</option>
												<option value="ward4">WARD 4</option>
												<option value="ward5">WARD 5</option>
												<option value="ward6">WARD 6</option>
												<option value="ward7">WARD 7</option>
												<option value="ward8">WARD 8</option>
											</select></div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-2 control-label">TOTAL AREA SIZE</label>
											<div class="col-sm-8">
												<input type="text" name="b_area" class="form-control1" id="focusedinput" placeholder="Default Input">
											</div>
											<div class="col-sm-2">
												<p class="help-block">Your help text!</p>
											</div>
										</div>
									<!--	<div class="form-group">
											<label class="col-sm-2 control-label">PASSWORD</label>
											<div class="col-sm-8">
												<input type="password" name="password" value="<?php  mt_rand(100000,999999); ?>" class="form-control1" id="inputPassword" placeholder="Password">
											</div>
										</div>
										!--->
										<div class="form-group">
											<label  class="col-sm-2 control-label">HOLDING NUMBER</label>
											<div class="col-sm-8">
												<input type="text" name="holding_no" class="form-control1" id=" " placeholder="Holding No.">
											</div>
										</div>
										<div class="form-group">
											<label  class="col-sm-2 control-label">BUILDING IMAGE</label>
											<div class="col-sm-8">
												<input type="file" name="b_image" class="form-control1" accept="image/*">
											</div>
										</div>
								       <div class="form-group">
											<label  class="col-sm-2 control-label">BUILDING DOCUMENTS</label>
											<div class="col-sm-8">
												<input type="file" name="b_doc_image" class="form-control1">
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-8">
											<input type="SUBMIT" value="Next" class="form-control1">
											</div>
										</div>
									</form>
								</div>
						</div>
					</div>





							</div>
							</div>
						</div>
			
			<!--/content-inner-->

			<!--/sidebar-menu-->
				<div class="sidebar-menu">
					<header class="logo1">
						<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> 
					</header>
						<div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
                           <div class="menu">
									<ul id="menu" >
										<li><a href="index.php"><i class="fa fa-tachometer"></i> <span>Home</span></a></li>
										 <li id="menu-academico" ><a href="#"><i class="fa fa-table"></i> <span> BUILDING </span> <span class="fa fa-angle-right" style="float: right"></span></a>
										   <ul id="menu-academico-sub" >
										   <li id="menu-academico-avaliacoes" ><a href="#">ADD</a></li>
											<li id="menu-academico-avaliacoes" ><a href="#">EDIT</a></li>
											<li id="menu-academico-boletim" ><a href="#">DELETE</a></li>
											<li id="menu-academico-boletim" ><a href="#">VIEW</a></li>
										  </ul>
										</li>
										 <li id="menu-academico" ><a href="#"><i class="fa fa-table"></i> <span> USER </span> <span class="fa fa-angle-right" style="float: right"></span></a>
										   <ul id="menu-academico-sub" >
										   <li id="menu-academico-avaliacoes" ><a href="#">ADD</a></li>
											<li id="menu-academico-avaliacoes" ><a href="#">EDIT</a></li>
											<li id="menu-academico-boletim" ><a href="#">DELETE</a></li>
											<li id="menu-academico-boletim" ><a href="#">VIEW</a></li>
										  </ul>
										</li>
										 <li id="menu-academico" ><a href="#"><i class="fa fa-table"></i> <span> TAX INFO </span> <span class="fa fa-angle-right" style="float: right"></span></a>
										   <ul id="menu-academico-sub" >
										   <li id="menu-academico-avaliacoes" ><a href="#">ADD</a></li>
											<li id="menu-academico-avaliacoes" ><a href="#">EDIT</a></li>
											<li id="menu-academico-boletim" ><a href="#">DELETE</a></li>
											<li id="menu-academico-boletim" ><a href="#">VIEW</a></li>
										  </ul>
										</li>
							<li id="menu-academico" ><a href="shoes.html"><i class="lnr lnr-book"></i> <span>TAX CIRCLE</span></a></li>
							<li id="menu-academico" ><a href="shoes.html"><i class="lnr lnr-book"></i> <span>SMS</span></a></li>
								<li id="menu-academico" ><a href="shoes.html"><i class="lnr lnr-book"></i> <span>NOTICE</span></a></li>
								<li id="menu-academico" ><a href="shoes.html"><i class="lnr lnr-book"></i> <span>PENALTY</span></a></li>
										
								  </ul>
								</div>
							  </div>
							  <div class="clearfix"></div>		
							</div>

		                   <script>
							var toggle = true;
										
							$(".sidebar-icon").click(function() {                
							  if (toggle)
							  {
								$(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
								$("#menu span").css({"position":"absolute"});
							  }
							  else
							  {
								$(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
								setTimeout(function() {
								  $("#menu span").css({"position":"relative"});
								}, 400);
							  }
											
											toggle = !toggle;
										});
							</script>


   </body>
</html>