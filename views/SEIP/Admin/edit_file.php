<?php
require_once("../../../vendor/autoload.php");

use App\BITM\SEIP\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div id='message'> $msg </div>";



$objAdmin = new \App\BITM\SEIP\Admin\Admin();
$objAdmin->setBuildingData($_GET);
$oneData = $objAdmin->view();
$upload_dir = "Upload/";



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User Info Editing Form</title>


    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>


    <style>


        .margin{

            margin-top: 5%;
        }

    </style>

</head>
<body class="background">

<div class="margin container" style="background-color:lightseagreen;color: #ffffff">
    <h1 class="display-1 text-center">Edit Image</h1>
</div>

<div class="panel container center_div">
    <form class="form-group" method="post" enctype="multipart/form-data" action="update_file.php">


        Choose Image Name:
        <input class="form-control" type="file" name="b_image" accept="image/*">
        <br>


        Choose Image Name:
        <input class="form-control" type="file" name="b_doc_image">
        <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id ?>">

        <input type="submit" class="btn bg-success btn-lg btn-block">



    </form>
    <?php

    //  $serial= 1;  ##### We need to remove this line to work pagination correctly.


    echo "<center>Building Image:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Building Document</center><br>";
    echo "
          <center> <img src='$upload_dir$oneData->b_image' width='150px' height='150px' class='img-rounded' alt ='Profile Picture'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           <img src='$upload_dir$oneData->b_doc_image' width='150px' height='150px' class='img-rounded' alt ='Doc Image'> </center>
              ";

    ?>
</div>
</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>

