<?php
require_once("../../../vendor/autoload.php");

$objAdmin = new \App\BITM\SEIP\Admin\Admin();

$allData = $objAdmin->index();

use App\BITM\SEIP\Message\Message;
use App\BITM\SEIP\Utility\Utility;
/*
if(!isset($_SESSION)){
    session_start();

}
$msg = Message::getMessage();
*/
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);

$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;


$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objAdmin->indexPaginator($page,$itemsPerPage);



$serial = (  ($page-1) * $itemsPerPage ) +1;



if($serial<1) $serial=1;
?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Holder List</title>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../../../resource/assets/bootstrap/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="../../../resource/assets/css/styleadmin.css" rel='stylesheet' type='text/css' />
    <!-- Graph CSS -->
    <link href="../../../resource/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- jQuery -->

    <!-- lined-icons -->
    <link rel="stylesheet" href="../../../resource/assets/css/icon-font.min.css" type='text/css' />

    <!-- //lined-icons -->
    <script src="../../../resource/assets/js/jquery-1.10.2.min.js"></script>



    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }


    </style>



</head>

<body>

<div class="header-section">
    <!-- top_bg -->
    <div class="top_bg">
        <div class="header_top">
            <div class="top_right">
                <ul>
                    <li><a href="../User/index.php">User</a></li>|
                    <li><a href="#">TITILE</a></li>|
                    <li><a href="#">TITILE</a></li>
                </ul>
            </div>
            <div class="top_left">
                <h2><span></span> Call us : 01670377276</h2>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

<div class="container">

    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'>  </div> </div>"; ?>



    <div class="content">
        <div class="women_main bg success" style="background-color: #0a2b1d">
            <!-- start content -->

            <div class="panel panel-widget forms-panel" style="background-color: #0a2b1d;color: white">
                <div class="form-title"style="background-color: #0a2b1d">
                    <center><h2>BUILDING INFORMATION INSERT</h2></center>
                </div>
            </div>
        </div>


    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th class="text-center">Select all  <input id="select_all" type="checkbox" value="select all"></th>


            <th class="text-center">Serial Number</th>
            <th class="text-center">Holder's Name</th>
            <th class="text-center">Building Name</th>
            <th class="text-center">Holding Number</th>
            <th class="text-center">Circle</th>
            <th class="text-center">Block</th>
            <th class="text-center">Ward</th>
            <th class="text-center">Action Buttons</th>
        </tr>

        <?php
        //  $serial= 1;  ##### We need to remove this line to work pagination correctly.


        foreach($someData as $oneData){ ########### Traversing $someData is Required for pagination  #############

            if($serial%2) $bgColor = "AZURE";
            else $bgColor = "#ffffff";

            echo "

                  <tr  style='background-color: $bgColor'>

                     <td style='padding-left: 6%'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>

                     <td style='width: 10%; text-align: center'>$serial</td>
                     <td>$oneData->first_name $oneData->last_name</td>
                     <td>$oneData->b_name</td>
                     <td>$oneData->holding_no</td>
                     <td>$oneData->circle</td>
                     <td>$oneData->block</td>
                     <td>$oneData->ward</td>

                     <td>
                       <a href='view.php?id=$oneData->id' class='btn btn-info'><span class='glyphicon glyphicon-eye-open'></span> View</a><br>
                     <a href='edit_building.php?id=$oneData->id' class='btn btn-success'><span class='glyphicon glyphicon-edit'></span> Edit Information</a><br>
                       <a href='edit_file.php?id=$oneData->id' class='btn btn-success'><span class='glyphicon glyphicon-edit'></span> Edit Files</a><br>
                       <a href='delete.php?id=$oneData->id' class='btn btn-danger'><span class='glyphicon glyphicon-eye-open'></span> Delete</a><br>
                     </td>
                  </tr>
              ";
            $serial++;
        }
        ?>

    </table>


    </form>
    </div>
    <!--  ######################## pagination code block#2 of 2 start ###################################### -->
    <div align="left" class="container">
        <ul class="pagination">

            <?php

            $pageMinusOne  = $page-1;
            $pagePlusOne  = $page+1;


            if($page>$pages) Utility::redirect("holderlist.php?Page=$pages");

            if($page>1)  echo "<li><a href='holderlist.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";


            for($i=1;$i<=$pages;$i++)
            {
                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

            }
            if($page<$pages) echo "<li><a href='holderlist.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

            ?>

            <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                <?php
                if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                ?>
            </select>
        </ul>
    </div>
    <!--  ######################## pagination code block#2 of 2 end ###################################### -->











    </div>



<!---------------CheckBox JavaScript !--------------------------->


    <script>
    jQuery(function($) {
        $('#message').fadeIn(500);
        $('#message').fadeOut (500);
        $('#message').fadeIn (500);
        $('#message').delay (2500);
        $('#message').fadeOut (2000);
    })

    $('#delete').on('click',function(){
        document.forms[1].action="deletemultiple.php";
        $('#multiple').submit();
    });



    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });







</script>
</body>
</html>