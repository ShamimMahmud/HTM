<?php
require_once("../../../vendor/autoload.php");

$objAdmin = new \App\BITM\SEIP\Admin\Admin();
$objAdmin->setBuildingData($_GET);
$oneData = $objAdmin->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Holder Details</title>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }

    </style>



</head>
<body class="background">
<br><br>

<div class="container">


    <div class="panel-heading" style="background-color:#8aa6c1;color: #ffffff">
        <h1 class="display-1 text-center"><?php echo $oneData->b_name;?></h1>
    </div>

    <table class="table table-striped table-bordered" width="40px" cellspacing="0px">
<?php

echo "
    <link rel='stylesheet' href='../../../resource/assets/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='../../../resource/assets/bootstrap/css/bootstrap-theme.min.css'>
    <script src='../../../resource/assets/bootstrap/js/bootstrap.min.js'></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }

    </style>

        <tr class='text-center'>
            <th class='text-center'>Holder's Name</th>
            <td>$oneData->first_name $oneData->last_name</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Building Name</th>
            <td>$oneData->b_name</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Area Name</th>
            <td>$oneData->area_name</td>
        </tr>

        <tr class='text-center'>
            <th class='text-center'>Circle Name</th>
            <td>$oneData->circle</td>
        </tr>

        <tr class='text-center'>
            <th class='text-center'>Block</th>
            <td>$oneData->block</td>
        </tr>

        <tr class='text-center'>
            <th class='text-center'>Ward</th>
            <td>$oneData->ward</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Building Type</th>
            <td>$oneData->b_type</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Tenant Type</th>
            <td>$oneData->tenant_type</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Area of Building</th>
            <td>$oneData->b_area</td>
        </tr>
         <tr class='text-center'>
            <th class='text-center'>Father's Name</th>
            <td>$oneData->father_name</td>
        </tr>
         <tr class='text-center'>
            <th class='text-center'>Mother's Name</th>
            <td>$oneData->mother_name</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Date of Birth</th>
            <td>$oneData->birth_date</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Gender</th>
            <td>$oneData->gender</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Email</th>
            <td>$oneData->email</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Address</th>
            <td>$oneData->address</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Mobile</th>
            <td>$oneData->mobile_no</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>NID</th>
            <td>$oneData->nid</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Building Image</th>
            <td>$oneData->b_image</td>
        </tr>
        <tr class='text-center'>
            <th class='text-center'>Document of Building</th>
            <td>$oneData->b_doc_image</td>
        </tr>


        ";
        ?>



    </table>

</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>