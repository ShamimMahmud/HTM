-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2017 at 04:06 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `htms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `admin_name` int(11) NOT NULL,
  `admin_password` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE `building` (
  `id` int(15) NOT NULL,
  `reg_date` date NOT NULL,
  `reg_no` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `b_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `area_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `b_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `tenant_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `block` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `circle` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `ward` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `b_area` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `holding_no` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `b_image` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `b_doc_image` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `building`
--

INSERT INTO `building` (`id`, `reg_date`, `reg_no`, `b_name`, `area_name`, `b_type`, `tenant_type`, `block`, `circle`, `ward`, `b_area`, `holding_no`, `b_image`, `b_doc_image`) VALUES
(12, '2017-03-17', '4564646', 'Shamim Manjil & Manj', 'Chittagong', 'TIN-SHED', 'RENTAL', 'A', 'circle1', 'ward1', '4500', '123456', '1488506736Lighthouse.jpg', '1488506736Tulips.jpg'),
(13, '2017-03-17', '456789123', 'Shakil Villa& Build', 'Dhaka', 'SEMI-PACCA', 'OWN USE', 'C', 'circle4', 'ward3', '5000', '500000', '1488506946Desert.jpg', '1488506946Chrysanthemum.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `circle`
--

CREATE TABLE `circle` (
  `id` int(11) NOT NULL,
  `circle_no` varchar(111) NOT NULL,
  `ward_no` varchar(111) NOT NULL,
  `percentage` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `circle`
--

INSERT INTO `circle` (`id`, `circle_no`, `ward_no`, `percentage`) VALUES
(1, 'Circle-1', 'Ward-1', '14%');

-- --------------------------------------------------------

--
-- Table structure for table `holder`
--

CREATE TABLE `holder` (
  `building_id` int(15) NOT NULL,
  `first_name` varchar(222) NOT NULL,
  `last_name` varchar(222) NOT NULL,
  `father_name` varchar(222) NOT NULL,
  `mother_name` varchar(222) NOT NULL,
  `birth_date` date NOT NULL,
  `gender` varchar(11) NOT NULL,
  `email` varchar(222) NOT NULL,
  `address` varchar(222) NOT NULL,
  `mobile_no` int(222) NOT NULL,
  `nid` int(222) NOT NULL,
  `holder_image` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `holder`
--

INSERT INTO `holder` (`building_id`, `first_name`, `last_name`, `father_name`, `mother_name`, `birth_date`, `gender`, `email`, `address`, `mobile_no`, `nid`, `holder_image`) VALUES
(12, 'Shamim', 'Mahmud', 'Md Nurullah', 'Rokeya Begum Chy', '2017-03-14', 'male', 'shamim10cse@gmail.com', 'Ctg', 1846028856, 2147483647, '1488506781Jellyfish.jpg'),
(13, 'Shakil', 'Mahmud', 'Nurullah', 'Rokeya', '0000-00-00', 'male', 'shakil@com', 'Dhaka502', 0, 502, '1488506996Penguins.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`) VALUES
(17, 'Test', 'User', 'tushar.chowdhury@gmail.com', '202cb962ac59075b964b07152d234b70', '01711111111', 'Chittagong', 'Yes'),
(18, 'sdjf', 'lksdjf', 'tusharbd@gmail.com', 'caf1a3dfb505ffed0d024130f58c5cfa', '5235', 'dfgdg', 'Yes'),
(19, 'asfds', 'sdfgs', 'x@y.z', '202cb962ac59075b964b07152d234b70', '4545', 'sfsj', '4ae15d1c46f25be8db9d07061463c5f0'),
(20, 'ro', 'dd', 'dfsdfs@fdf.com', '8d70d8ab2768f232ebe874175065ead3', '3432', 'sflsdkfop', 'da754e9a44de2aed0b4e0d94b12e12f8'),
(21, 'sec', 'ss', 'ss@dff.com', '4a352700428c7192e05f118686693e92', '33434', 'fgsfg', 'ce63736a6207386652403cd8b47e4919'),
(22, 'one', 'two', 'three@yahoo.com', 'c446348099d4f2bcfdb26a96330b46e5', '555555', 'six', 'd312843319ad2ba52673cd7937386985'),
(23, 'firs', 'last', 'email', 'e10adc3949ba59abbe56e057f20f883e', '123', '33f', '1bc0f989c0fcce2afe76b06ac4b67014'),
(24, 'Roki', 'Das Gupta', 'rokidasgupta@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'High Level Road', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `circle`
--
ALTER TABLE `circle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holder`
--
ALTER TABLE `holder`
  ADD UNIQUE KEY `building_id` (`building_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `building`
--
ALTER TABLE `building`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `circle`
--
ALTER TABLE `circle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `holder`
--
ALTER TABLE `holder`
  ADD CONSTRAINT `holder_ibfk_1` FOREIGN KEY (`building_id`) REFERENCES `building` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
