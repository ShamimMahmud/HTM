<?php
/**
 * Created by PhpStorm.
 * User: roki
 * Date: 23-Feb-17
 * Time: 10:15 AM
 */

namespace App\BITM\SEIP\Admin;

use App\BITM\SEIP\Message\Message;
use App\BITM\SEIP\Utility\Utility;

use App\BITM\SEIP\Model\Database as DB;
use PDO;
use PDOException;

class Admin extends DB
{
    private $id;
    private $reg_date;
    private $reg_no;
    private $b_name;
    private $area_name;
    private $b_type;
    private $tenant_type;
    private $block;
    private $circle;
    private $ward;
    private $b_area;
    private $holding_no;
    private $b_image;
    private $b_doc_image;

    private $first_name;
    private $last_name;
    private $father_name;
    private $mother_name;
    private $birth_date;
    private $gender;
    private $mobile_no;
    private $email;
    private $address;
    private $NID;
    private $get_id;
    private $fetch_id;
    private $building_id;
    private $holder_image;


    private $circle_no;
    private $ward_no;
    private $percentage;

    public function setBuildingData($postData){

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('reg_date',$postData)){
            $this->reg_date = $postData['reg_date'];
        }

        if(array_key_exists('reg_no',$postData)){
            $this->reg_no = $postData['reg_no'];
        }

        if(array_key_exists('b_name',$postData)){
            $this->b_name = $postData['b_name'];
        }
        if(array_key_exists('area_name',$postData)){
            $this->area_name = $postData['area_name'];
        }

        if(array_key_exists('b_type',$postData)){
            $this->b_type = $postData['b_type'];
        }

        if(array_key_exists('tenant_type',$postData)){
            $this->tenant_type = $postData['tenant_type'];
        }

        if(array_key_exists('block',$postData)){
            $this->block = $postData['block'];
        }

        if(array_key_exists('circle',$postData)){
            $this->circle = $postData['circle'];
        }
        if(array_key_exists('ward',$postData)){
            $this->ward = $postData['ward'];
        }

        if(array_key_exists('b_area',$postData)){
            $this->b_area = $postData['b_area'];
        }
        if(array_key_exists('holding_no',$postData)){
            $this->holding_no = $postData['holding_no'];
        }
        if(array_key_exists('b_image',$postData)){
            $this->b_image = $postData['b_image'];
        }
        if(array_key_exists('b_doc_image',$postData)){
            $this->b_doc_image = $postData['b_doc_image'];
        }


    }

    public function setUserData($postData){


        if(array_key_exists('first_name',$postData)){
            $this->first_name = $postData['first_name'];
        }

        if(array_key_exists('last_name',$postData)){
            $this->last_name = $postData['last_name'];
        }

        if(array_key_exists('father_name',$postData)){
            $this->father_name = $postData['father_name'];
        }
        if(array_key_exists('mother_name',$postData)){
            $this->mother_name = $postData['mother_name'];
        }

        if(array_key_exists('birth_date',$postData)){
            $this->birth_date = $postData['birth_date'];
        }

        if(array_key_exists('gender',$postData)){
            $this->gender = $postData['gender'];
        }

        if(array_key_exists('mobile_no',$postData)){
            $this->mobile_no = $postData['mobile_no'];
        }

        if(array_key_exists('email',$postData)){
            $this->email = $postData['email'];
        }
        if(array_key_exists('address',$postData)){
            $this->address = $postData['address'];
        }

        if(array_key_exists('NID',$postData)){
            $this->NID = $postData['NID'];
        }
        if(array_key_exists('holding_no',$postData)){
            $this->holding_no = $postData['holding_no'];
        }

        if(array_key_exists('building_id',$postData)){
            $this->building_id = $postData['building_id'];
        }
        if(array_key_exists('holder_image',$postData)){
            $this->holder_image = $postData['holder_image'];
        }
    }

    public function setCircleData($postData)
    {

        if (array_key_exists('circle_no', $postData)) {
            $this->circle_no = $postData['circle_no'];
        }

        if (array_key_exists('ward_no', $postData)) {
            $this->ward_no = $postData['ward_no'];
        }

        if (array_key_exists('percentage', $postData)) {
            $this->percentage = $postData['percentage'];
        }

    }

    public function store(){

        $arrData = array($this->reg_date,$this->reg_no,$this->b_name,$this->area_name,$this->b_type,$this->tenant_type,$this->block,$this->circle,$this->ward,$this->b_area,$this->holding_no,$this->b_image,$this->b_doc_image);


        $sql = "INSERT into building(reg_date,reg_no,b_name,area_name,b_type,tenant_type,block,circle,ward,b_area,holding_no,b_image,b_doc_image) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $STH = $this->conn->prepare($sql);

        $result =$STH->execute($arrData);



        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        $_SESSION=$_POST;

        Utility::redirect('createUser.php');


    }


    public function getBuildingId(){


        $sql = 'SELECT id FROM building ORDER BY id DESC LIMIT 1';

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();
    }



    public function storeUser(){



        $arrData = array($this->building_id,$this->first_name,$this->last_name,$this->father_name,$this->mother_name,$this->birth_date,$this->gender,$this->mobile_no,$this->email,$this->address,$this->NID,$this->holder_image);

        $sql = "INSERT into holder(building_id,first_name,last_name,father_name,mother_name,birth_date,gender,mobile_no,email,address,nid,holder_image) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

        $STH = $this->conn->prepare($sql);



        $result = $STH->execute($arrData);



        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('../index.php');


    }

    public function storeCircle(){

        $arrData = array($this->circle_no,$this->ward_no,$this->percentage);

        $sql = "INSERT into circle(circle_no,ward_no,percentage) VALUES(?,?,?)";

        $STH = $this->conn->prepare($sql);

        // Utility::d($arrData);
        // Utility::d($STH);

        $result = $STH->execute($arrData);


        //Utility::dd($result);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('../index.php');


    }

    public function index(){

        $sql = 'select * from building INNER JOIN holder ON building.id=holder.building_id';

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function indexPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from building INNER JOIN holder ON building.id=holder.building_id LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "select * from building INNER JOIN holder ON building.id=holder.building_id";

        }

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }

    public function view(){


       // $sql = 'SELECT * FROM (SELECT * FROM building INNER JOIN holder ON building.holding_no=holder.holding_no) WHERE holding_no='.$this->holding_no;

        $sql = 'SELECT * FROM building,holder WHERE building.id=holder.building_id AND building.id='.$this->id;

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();



    }

    public function updateBuilding(){

        $arrData = array($this->reg_date,$this->reg_no,$this->b_name,$this->area_name,$this->b_type,$this->tenant_type,$this->block,$this->circle,$this->ward,$this->b_area,$this->holding_no);

        $sql = "UPDATE  building SET reg_date=?,reg_no=?,b_name=?,area_name=?,b_type=?,tenant_type=?,block=?,circle=?,ward=?,b_area=?,holding_no=? WHERE id=".$this->id;


        $STH = $this->conn->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");


        Utility::redirect('holderlist.php');


    }

    public function updateBuildingFiles(){

        $arrData = array($this->b_image,$this->b_doc_image);

        $sql = "UPDATE  building SET b_image=?,b_doc_image=? WHERE id=".$this->id;


        $STH = $this->conn->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('holderlist.php');


    }



    public function updateUser(){

        $arrData = array($this->building_id,$this->first_name,$this->last_name,$this->father_name,$this->mother_name,$this->birth_date,$this->gender,$this->mobile_no,$this->email,$this->address,$this->NID);

        $sql = "UPDATE  holder SET building_id=?,first_name=?,last_name=?,father_name=?,mother_name=?,birth_date=?,gender=?,mobile_no=?,email=?,address=?,nid=? WHERE building_id=".$this->id;


        $STH = $this->conn->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('holderlist.php');


    }
}